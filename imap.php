<?php
/*
*  imap.php  Use the arrays to configure a list of mailboxes to connect to.
*  html output will be a table with one column per mailbox with configurable color,text and an "UNSEEN" message count
*  Only tested with gmail
*  Page automatically refreshes every 5 seconds via javascript at end of the body 
*  requires php-imap
*/


//Assumes same location for each mailbox could be changed to an array. ssl and novalidate-cert were required for gmail may not be elsewhere
$mailBox="{imap.gmail.com:993/imap/ssl/novalidate-cert}Inbox";

// Arrays Below should have equal number of items in them
$userNames=array("AAAAAA@gmail.com","BBBBBBB@gmail.com");
$passWords=array("PASSWORD_A","PASSWORD_B");
$displayNames=array("MAILBOX YY","MAILBOX XX");
$displayColors=array("red","blue");
$displayLevels=array(0,0);


$accounts = count($userNames);
if 
(($accounts != count($passWords))||
($accounts != count($displayNames))||
($accounts != count($displayColors))||
($accounts != count($displayLevels)))
{
    exit("Array counts not matching");
}

?>
<html>
<head>
<style>
table,th,td
{
border:1px solid black;
text-align:center;
}
</style>
</head>
<body>
<table width="100%" height="100%">
<tr height="100%">
<?php

$colWidth = 100/$accounts;
for($acc = 0; $acc <$accounts; $acc++)
{
if ($mbox=imap_open( $mailBox,$userNames[$acc], $passWords[$acc] )) 
{
    //echo "Connected\n";
    $result = imap_search($mbox, 'UNSEEN');
    $XCount = count($result);
    if($XCount>$displayLevels[$arr])
    {
        echo "<td width=\"".$colWidth."%\"  style=\"background-color:".$displayColors[$acc]."\">".$displayNames[$acc]."<br>".$XCount."</td>";
    }
    else
    {
        echo "<td width=\"".$colWidth."%\">".$displayNames[$acc]."<br>0</td>";
    }
} 
else 
{ 
    exit ("Can't connect: " . imap_last_error() ."Mailbox ".$userNames[$acc]."\n");  echo "FAIL!\n";  
} 
}
?>

</tr>
</table>
<script type='text/javascript'>
// Set the page to refresh every 5 seonds
setTimeout(function(){
   window.location.reload(1);
}, 5000);
</script>
</body>
</html>
